<img src ="./assets/Flat.jpg" style="width: 50%; hieght: 50%, backgroundColor: black" />

# Vue Native Flat App

Based on [Vue Native](https://vue-native.io/) and [NativeBase](http://nativebase.io/), [Vue Native Flat App](https://gitstrap.com/Vue-Native/VueNative-FlatApp) is a well structured, responsive iOS and Android app having fully customizable pages along with a rich collection of UI elements specifically for an ideal flat themed app to help you quickly get started on your next project.

[Vue Native](https://vue-native.io/) is a mobile framework to build truly native mobile app using [Vue.js](https://vuejs.org/). It is designed to connect React Native and Vue.js. Vue Native is a wrapper around React Native APIs, which allows you to use Vue.js to compose rich mobile User interfaces.

[NativeBase](http://nativebase.io/) is a free and open source framework that enables developers to build high-quality mobile apps using [React Native](https://facebook.github.io/react-native/) iOS and Android apps with a fusion of ES6. NativeBase builds a layer on top of React Native that provides you with basic set of components for mobile application development.

[Vue Native Flat App](https://gitstrap.com/Vue-Native/VueNative-FlatApp) is basically a style of interface design emphasizing minimum use of stylistic elements. It is focused on a minimalist use of simple elements, typography and flat colors. Developers prefer flat design as it allows interface designs to be more streamlined and efficient. It is easier to quickly convey information while still looking visually appealing and approachable. With minimal design elements, applications are able to load faster and resize easily, and still look sharp on high-definition screens.

[Vue Native Flat App](https://gitstrap.com/Vue-Native/VueNative-FlatApp) was built using Vue Native CLI, which internally uses [Expo CLI](https://docs.expo.io/versions/latest/workflow/expo-cli/). Expo allows you to work with all of the components and APIs in React Native, as well as additional Expo-specific APIs.
