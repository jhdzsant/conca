# Release Notes

<hr />

### v1.2.0

#### Upgrades

- Upgraded to [Expo SDK 38](https://docs.expo.io/versions/v38.0.0/) and [react-native 0.62](https://github.com/facebook/react-native/releases/tag/v0.62.2)
- Upgraded to [native-base 2.13.13](https://github.com/GeekyAnts/NativeBase/releases/tag/v2.13.13)
- Moved away from `react-native-carousel-view` and replaced with [react-native-snap-carousel 3.9.1](https://github.com/archriss/react-native-snap-carousel/releases/tag/v3.9.1)

<hr />

### v1.1.0

#### Upgrades

- Upgraded to [Expo SDK 35](https://docs.expo.io/versions/v35.0.0/sdk/overview/) and [react-native 0.59](https://github.com/facebook/react-native/releases/tag/v0.59.8)
- Upgraded to [Vue Native Core 0.1.4](https://github.com/GeekyAnts/vue-native-core/releases/tag/v0.1.4)
- Upgraded to [vue-native-router 0.1.1](https://github.com/GeekyAnts/vue-native-router/releases/tag/v0.1.1)
- Upgraded to [native-base 2.13](https://github.com/GeekyAnts/NativeBase/releases/tag/v2.13.8)
- Upgraded to [react-native-modalbox 2.0.0](https://github.com/maxs15/react-native-modalbox/releases/tag/v2.0.0)
- Upgraded to [react-native-keyboard-aware-scroll-view 0.9.1](https://github.com/APSL/react-native-keyboard-aware-scroll-view/releases/tag/v0.9.1)

#### Improvements

- ESLint integration. Lint code with `npm run lint`
- Code formatting with Prettier. Format code with `npm run lint:fix`

<hr />

### v1.0.0

#### Features

- Integrated [react-native](https://github.com/facebook/react-native) 0.55.2
- Integrated [native-base](https://github.com/GeekyAnts/NativeBase) 2.6.1
- Integrated [vue-native-core](https://github.com/GeekyAnts/vue-native-core) 0.0.7
- Integrated [vue-native-helper](https://www.npmjs.com/package/vue-native-helper) 0.0.8
- Integrated [vue-native-router](https://www.npmjs.com/package/vue-native-router) 0.0.1-alpha.3
- Integrated [ React Native Vector Icons](https://github.com/lelandrichardson/react-native-parallax-view) 0.21.3
- Integrated `react-native-calendars` 1.19.3
- Integrated `react-native-modalbox` 1.5.0
