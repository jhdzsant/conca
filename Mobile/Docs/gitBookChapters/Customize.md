## Theming Your App

Customizing the app will be a cakewalk for you. A well prepared code structure makes it simple to dig through the code and customize it with ease.

- The theme has categorized its screens into different sections.
- The theme has a separate file containing color schemes for different sections.
- The theme strictly follows programming ethics, hence modifying logo at different locations for your app becomes very simple.
- The theme also allows you to customize the name of your app.

### Changing a component's color

To customize any component of the app is very easy. Follow the [NativeBase](https://docs.nativebase.io/Customize.html#Customize) procedure to do so.

### Changing the app icon

[Vue Native](https://vue-native.io/) internally uses [Expo](https://docs.expo.io/versions/latest/). With Expo, changing the app icon is much easier than with React Native apps. You just need to give local path or remote URL of an image you want to use as app icon to the [icon](https://docs.expo.io/versions/latest/workflow/configuration/#icon) attribute in `app.json`.

### Renaming the app

Renaming any [Expo](https://docs.expo.io/versions/latest/) app is very easy. You just need to rename the [name](https://docs.npmjs.com/files/package.json#name) attribute in your `package.json`.

### Changing the URL name for publishing with Expo

To change the app name for the Expo URL, you just need to rename the [slug](https://docs.expo.io/versions/latest/workflow/configuration/#slug) attribute in `app.json` file.
