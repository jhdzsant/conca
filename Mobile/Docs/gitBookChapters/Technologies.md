# Technologies Used

The components of the App are built using the core components of [Vue Native](https://vue-native.io/) and [Native Base](http://nativebase.io/). The theme also incorporates the latest versions of various other technologies.

- Vue Native
- NativeBase v2.x
- Vue Native Router
- React Native Easy Grid
- Vuelidate

## [Vue Native​](https://vue-native.io/)

Vue Native is a mobile framework to build truly native mobile app using Vue.js. Its is designed to connect React Native and Vue.js. Vue Native is a wrapper around React Native APIs, which allows you to use Vue.js to compose rich mobile user interfaces.

## [React Native](https://facebook.github.io/react-native/)

React Native helps in making the development work easier and allowing the developers to focus on the core app features in every new release. It is the fastest-developing mobile app development that essentially permits you to create an isolated product with frequent outcomes. The hymn of React Native is **learn once, write anywhere**. React Native takes charge of the view controllers and programatically generates native views using javascript. This means that you can have all the speed and power of a native application, with the ease of development that comes with React.

## [NativeBase](https://nativebase.io/)

NativeBase is an open source framework from the team of [StrapMobile](https://strapmobile.com/). This framework enable developers to build high-quality mobile apps using [React Native](https://facebook.github.io/react-native/) iOS and Android apps with a fusion of [ES6](https://github.com/lukehoban/es6features). NativeBase builds a layer on top of React Native that provides you with basic set of components for mobile application development. The applications stack of components is built using native UI components and because of that, there are no compromises with the User Experience of the applications. NativeBase is targeted specially on the look and feel, and UI interplay of your app. NativeBase without a doubt fits in well with mobile applications which cut downs one huge part of your app - The Front end.

### Quick links to NativeBase

- [GitHub](https://github.com/GeekyAnts/NativeBase)
- [NativeBase Components](http://docs.nativebase.io/Components.html#Components)
- [Documentation](https://docs.nativebase.io/)
- [Blogs](https://blog.nativebase.io/)

## ​[Vue Native Router​](https://vue-native.io/docs/vue-native-router.html)

[Vue Native Router​](https://vue-native.io/docs/vue-native-router.html) allow you to define your application's navigation structure. Vue Native Router also renders common elements such as headers and tab bars which you can configure. Vue Native Router is based on [React Navigation](https://reactnavigation.org/).​

- **Stack Navigator**

  Renders one screen at a time and provides transitions between screens. Each new screen is put on the top of the stack and going back removes a screen from the top of the stack.

- **Tab Navigator**

  Renders a tab bar that lets the user switch between several screens

- **Drawer Navigator**

  Provides a drawer that slides in from the left of the screen

### [Vuelidate​](https://vuelidate.netlify.com/)

Vuelidate is a simple, lightweight model-based validation for Vue.js. It can be seamlessly used with Vue Native for providing basic or complex validations in your Vue Native forms. The validations are completely decoupled from the template and are defined in the data model, providing a clean and elegant solution to validation in Vue Native.
