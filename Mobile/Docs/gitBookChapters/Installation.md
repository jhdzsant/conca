# Installation

Run the following commands on your terminal to setup Vue Native Flat App on your system:

#### Get the code

- Option 1: Download in ZIP format

  Not familiar with Git? Click [here](https://gitstrap.com/Vue-Native/VueNative-FlatApp/repository/archive.zip?ref=master) to download Vue Native Flat App. Extract the contents of ZIP file after downloading. Downloading ZIP file does not help you to sync with further updates of the app.

- Option 2: Clone the repository

  To setup Vue Native Flat App on your system, and sync your app with constant updates, clone the repo. Click [here](https://gitstrap.com/Vue-Native/VueNative-FlatApp) to clone the app using GitStrap.

#### Install packages

```sh
$ cd VueNative-FlatApp
$ npm install
```

#### Run on iOS

- Running on a physical device:
  1. Run `npm start` in your terminal
  2. Scan the QR code in your Expo app
- Running in a simulator:
  - Run `npm run ios` in your terminal

#### Run on Android

- Running on a physical device:
  1. Run `npm start` in your terminal
  2. Scan the QR code in your Expo app
- Running in an emulator:
  1. Make sure you have an Android emulator installed and running
  2. Run `npm run android` in your terminal
