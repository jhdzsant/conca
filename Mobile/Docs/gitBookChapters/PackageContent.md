## Package Contents

1. **Vue Native app**: The Vue Native version of the Flat App theme
2. **Documentation**: The GitBook source code for this documentation

#### System Requirements

- Globally installed [node](https://nodejs.org/en/download/) >= 10
- Globally installed [npm](https://docs.npmjs.com/cli/install) >= 6
- Globally installed [Expo CLI](https://docs.expo.io/versions/latest/workflow/expo-cli/)
