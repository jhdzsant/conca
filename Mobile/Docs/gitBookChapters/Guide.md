# Start Building Your App
This App guides you throughout building your app, providing the steps and procedure to customize.

Contents discussed in this section:

- How to add a new component
- How to add new styles

#### How to add new component
Create a new folder, say `NewComponent` and place it under `/src/screens`.

Create a new file `index.vue`, within this folder.

Start creating the component that you wish to create. To use Native Base components, use prefix `nb-` before the component you wish to use.

```html
<template>
  <nb-container />
</template>
```

#### How to add styles to your component

Create a `<style></style>` tag in your `index.vue`.

Use the class name of the component used and style it as you wish.

```html
<template>
  <nb-container class="container-style" />
</template>

<style>
.container-style {
  flex: 1;
  margin-bottom: 20;
}
</style>
```

If you wish to define styles that are dependent on a React Native API like setting the height of a component based on the Dimensions API, use it in the `<script></script>` tag part of the file. Declare the style as the component's `data`, and then bind the style object to the component in your template.

```html
<template>
  <nb-container
    class="container-style"
    :style="style.containerStyle"
  />
</template>

<script>
import { Dimensions } from 'react-native';
const deviceHeight = Dimensions.get('window').height;
export default {
  data() {
    return {
      style: {
        containerStyle: {
          height: deviceHeight
        }
      }
    };
  }
}
</script>

<style>
.container-style {
  flex: 1;
  margin-bottom: 20;
}
</style>
```
