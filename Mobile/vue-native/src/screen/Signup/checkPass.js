class checkPass{
  CheckPassword(inputtxt) 
  { 
    //aqui colocamos el Regex  que evaalua si la contraseña que se coloco es correcta de acuerdo a los
    //estandares de seguridad propuestos (minusculas,MAYUSCULAs,numeros y signos)
    
    //var passw= /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
    
    //La contraseña debe tener al entre 8 y 16 caracteres, al menos un dígito, al menos una minúscula y al menos una mayúscula.
    //NO puede tener otros símbolos.
    var passw= /^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
    
    if(inputtxt.match(passw)) 
    { 
    //alert('Correct')
    return true;
    }
    else
    { 
    //alert('Wrong...!')
    return false;
    }
  }

}
export default new checkPass();