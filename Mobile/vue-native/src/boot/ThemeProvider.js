import { StyleProvider } from "native-base";
import React, { Component } from "react";
import PropTypes from "prop-types";

import getTheme from "../../native-base-theme/components";
import variables from "../../native-base-theme/variables/commonColor";

export default class ThemeProvider extends Component {
  static propTypes = {
    children: PropTypes.oneOfType([
      PropTypes.node,
      PropTypes.arrayOf(PropTypes.node)
    ]).isRequired
  };

  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
        {this.props.children}
      </StyleProvider>
    );
  }
}
