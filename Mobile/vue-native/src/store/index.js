import Vue from "vue-native-core";
import Vuex from "vuex";
import * as actions from "./actions";
import * as mutations from "./mutations";


Vue.use(Vuex);

const store = new Vuex.Store({
  actions,
  mutations,
  state: {
    activeType: "posts",
    posts: [],
    loadingPosts: false,
    usuario: {},
    calificacion: {},
    countCalificar: 0,
    tarjetasCalifica: [],
    menu: {
      origen: "Home",
      bonton: false,
    },
    vivLugar: [],
    planLugares: [],
    categoriaPadreId: 0,
    imagen: "",
    geoZona: {},
    Usuarios: []
  }
});

export default store;
