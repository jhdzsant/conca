// ensure data for rendering given list type
export function FETCH_LIST_DATA({ commit }, { posts }) {
  commit("FETCHING_LISTS");
  commit("SET_POSTS", {
    posts
  });

}

export function SiguienteAction({ commit }) {
  console.log("entroaction");
  commit("NEXT_CARD");
}