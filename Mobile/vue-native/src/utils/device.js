import { Platform } from "react-native";
import * as Device from "expo-device";

export const iPhonesWithNotch = [
  "iPhone 11",
  "iPhone 11 Pro",
  "iPhone 11 Pro Max",
  "iPhone X",
  "iPhone XS",
  "iPhone XS Max",
  "iPhone XR",
  "iPhone 12 mini",
  "iPhone 12",
  "iPhone 12 Pro",
  "iPhone 12 Pro Max",
];

export function iPhoneHasNotch() {
   deviceName = '';
  if(Device.isDevice === true){
    deviceName = Device.modelName; 
  }else{
    deviceName = Device.deviceName
  }
  if (Platform.OS !== "ios" || !deviceName) return false;

  return (
    iPhonesWithNotch.findIndex(
      item => item.toLowerCase() === deviceName.toLowerCase()
    ) !== -1
  );
}
