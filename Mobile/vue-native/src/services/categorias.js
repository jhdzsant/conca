import http from "../http-common";

class CategoriasServices {
  getAll() {
    return http.get("/categoria");
  }
  hijos(id) {
    return http.get(`/categoria/hijos/${id}`);
  }
  showpadre(id){
    return http.get(`/categoria/showpadre/${id}`);
  }
}
     
export default new CategoriasServices();
