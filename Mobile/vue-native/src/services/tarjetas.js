import http from "../http-common";

class tarjetas {
    get(id, userId,zonaId) {
        return http.get(`lugares/tarjetas/${id}/${userId}/${zonaId}`);
    }

    getCount(userId,zonaId) {
        return http.get(`lugares/tarjetasCount/${userId}/${zonaId}`);
    }
}


export default new tarjetas();
