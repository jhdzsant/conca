import http from "../http-common";

class LugaresServcio {
  getAll() {
    return http.get("/Imagenes");
  }
  lugaresimagen(id,zonaid) {
    return http.get(`/lugares/lugaresimagen/${id}/${zonaid}`);
  }

  create(data) {
    return http.post("/Imagenes", data);
  }

  update(id, data) {
    return http.put(`/Imagenes/${id}`, data);
  }

  delete(id) {
    return http.delete(`/Imagenes/${id}`);
  }
  lugaresImagenCalificacion(id,usuarioId){
    return http.get(`/lugares/lugaresImagenCalificacion/${id}/${usuarioId}`);
  }

  lugaresBuckeetList(usuarioId){
    return http.get(`/lugares/lugaresImagenBucketList/${usuarioId}`);
  }

  lugaresDistancia(id,latitud,longitud,categoriaId,ZonaId){
    // console.log(id)
    // console.log(latitud)
    // console.log(longitud)
    // console.log(categoriaId)
    // console.log(ZonaId)
    return http.get(`/lugares/lugaresDistancia/${id}/${latitud}/${longitud}/${categoriaId}/${ZonaId}`);
  }

  GetImagenesxLugar(id) {
    console.log(id);
    return http.get(`/Imagenes/Imagenesxlugar/${id}`);
  }
}
     
export default new LugaresServcio();
