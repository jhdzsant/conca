import http from "../http-common";

class zona {
  getAll() {
    return http.get("/zona");
  }
  get(id) {
    return http.get(`/zona/${id}`);
  }

  create(data) {
    return http.post("/zona", data);
  }

  update(id, data) {
    return http.put(`/zona/${id}`, data);
  }

  delete(id) {

    return http.delete(`/zona/${id}`);
  }
}

     
export default new zona();
