import http from "../http-common";

class UserServices {
  getAll() {
    return http.get("/usuarios");
  }

  checkData(data) {
    console.log(`/usuarios/checkdata/${data.usermail}/`)
    return http.get(`/usuarios/checkdata/${data.usermail}/`);
  }

  create(data) {
    return http.post("/usuarios", data);
  }

  update(id, data) {
    return http.put(`/usuarios/${id}`, data);
  }
  
  updatePass(data) {
    return http.put(`/usuarios/updpass`, data);
  }

  delete(id) {
    return http.delete(`/usuarios/${id}`);
  }

  login(data) {
    console.log(data);
    return http.post(`/login?contrasenia=${data.contrasenia}&usuario=${data.usuario}`);
  }

  SendEmail(user){
    return http.get(`/usuarios/SendEmail/${user}/`);
  }
}
     

export default new UserServices();
