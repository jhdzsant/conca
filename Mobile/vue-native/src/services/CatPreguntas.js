import http from "../http-common";

class CatPreguntasServices {
  getAll() {
    return http.get("/catpreguntas");
  }
  getPreguntas(id, tipo){
    return http.get(`/catpreguntas/${id}/${tipo}`);
  }
  create(data) {
    return http.post("/califusuarios", data);
  }
  avg(id){
    return http.get(`/califusuarios/avg/${id}`);
  }
  vivirlugar(id){
    return http.get(`/califusuarios/vivirlugar/${id}`);
  }
  planlugar(id){
    console.log("entro");
    return http.get(`/califusuarios/planlugar/${id}`);
  }
}

export default new CatPreguntasServices();