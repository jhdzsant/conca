import http from "../http-common";

class calificacion {
  getAll() {
    return http.get("/calificacion");
  }
  get(id) {
    return http.get(`/calificacion/${id}`);
  }

  create(data) {
    return http.post("/calificacion", data);
  }

  update(id, data) {
    return http.put(`/calificacion/${id}`, data);
  }

  delete(id) {

    return http.delete(`/calificacion/${id}`);
  }
  CalificacionUsuario(data){
    return http.put('/calificacion/CalificacionUsuario', data);
  }
  getCalificacionUsuario(id,usuarioid){
    console.log("id: "+ id + " userid "+usuarioid);
    return http.get(`/calificacion/CalificacionUsuario/${id}/${usuarioid}`);
  }
}

     
export default new calificacion();
