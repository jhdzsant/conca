import http from "../http-common";

class UsuarioLugares {
    cantidadLugares(id) {
        return http.get(`/usuariolugares/countvisitados/${id}`);
    }
    post(data){
        return http.post("/usuariolugares",data);
    }
}


export default new UsuarioLugares();
