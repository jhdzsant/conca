import http from "../http-common";

class configuracionService {
  listado(clave) {
    return http.get(`/configuracion/listado/${clave}`);
  }
}
     
export default new configuracionService();
