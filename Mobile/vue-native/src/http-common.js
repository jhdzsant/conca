import axios from "axios";

export default axios.create({
  // baseURL: "https://localhost:44369/api",
  //baseURL: "https://localhost:5001/api",
  baseURL: "http://api-test-conca.us-east-1.elasticbeanstalk.com/api",
  headers: {
    "Content-type": "application/json",
  }
});
  