import variable from "./../variables/platform";

export default (variables = variable) => {
  const tabTheme = {
    flex: 1,
    backgroundColor: "#c3c3c3"
  };

  return tabTheme;
};
