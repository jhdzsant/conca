import http from "../http-common";

class ContactosClienteService {
  getAll(id) {
    return http.get(`/ContactosCliente/${id}`);
  }
  
  create(data) {
    return http.post("/ContactosCliente", data);
  }

  update(id, data) {
    return http.put(`/ContactosCliente/${id}`, data);
  }

  delete(id) {
    return http.delete(`/ContactosCliente/${id}`);
  }
}

export default new ContactosClienteService();