import http from "../http-common";

class lugaresCategoriaService {
  getAll(id) {
    return http.get(`/lugaresCategorias/${id}`);
  } 
  getSpecific(id) {
    return http.get(`/lugaresCategorias/${id}`);
  }   
  create(data) {
    console.log(data);
    return http.post("/lugaresCategorias", data);
  }

  update(id, data) {
    return http.put(`/lugaresCategorias/${id}`, data);
  }

  delete(id) {
    return http.delete(`/lugaresCategorias/${id}`);
  }
}

export default new lugaresCategoriaService();