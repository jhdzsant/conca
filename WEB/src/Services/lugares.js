import http from "../http-common";

class LugaresService {
  getAll() {
    return http.get("/lugares");
  } 
  getSpecific(id) {
    return http.get(`/lugares/${id}`);
  }   
  create(data) {
    console.log(data);
    return http.post("/lugares", data);
  }

  update(id, data) {
    return http.put(`/lugares/${id}`, data);
  }

  delete(id) {
    return http.delete(`/lugares/${id}`);
  }
  getespecificlugar(id){    
    return http.get(`/lugares/especificlugar/${id}`);
  }
}

export default new LugaresService();