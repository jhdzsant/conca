import http from "../http-common";

class ImagenesService {
  getAll(id) {
    return http.get(`/Imagenes/${id}`);
  }
  
  create(data) {
    return http.post("/Imagenes", data);
  }

  update(id, data) {
    return http.put(`/Imagenes/${id}`, data);
  }

  delete(id) {
    return http.delete(`/Imagenes/${id}`);
  }
}

export default new ImagenesService();