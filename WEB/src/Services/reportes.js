import http from "../http-common";

class Reporte {
  clientes() {
    return http.get("/clientes");
  } 
  Calificacion() {
    return http.get("/Calificacion");
  } 
  CalifUsuarios() {
    return http.get("/CalifUsuarios");
  } 
  Categorias() {
    return http.get("/Categoria");
  }  
  CatPreguntas() {
    return http.get("/CatPreguntas");
  } 
  ContactosCliente() {
    return http.get("/ContactosCliente");
  } 
  Lugares() {
    return http.get("/Lugares");
  }
  UsuarioLugares() {
    return http.get("/UsuarioLugares");
  }
  Usuarios() {
    return http.get("/Usuarios");
  }
  Zona() {
    return http.get("/Zona");
  }     
  LugaresCategorias(){
    return http.get("/LugaresCategorias");
  }
}

export default new Reporte();