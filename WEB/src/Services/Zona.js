import http from "../http-common";

class zonaService {
  getAll() {
    return http.get("/zona");
  } 
  getSpecific(id) {
    return http.get(`/zona/${id}`);
  }   
  create(data) {
    console.log(data);
    return http.post("/zona", data);
  }

  update(id, data) {
    return http.put(`/zona/${id}`, data);
  }

  delete(id) {
    return http.delete(`/zona/${id}`);
  }
}

export default new zonaService();