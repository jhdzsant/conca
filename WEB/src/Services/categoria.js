import http from "../http-common";

class CategoriaService {
  getAll() {
    return http.get("/categoria");
  }
  
  create(data) {
    return http.post("/categoria", data);
  }

  update(id, data) {
    return http.put(`/categoria/${id}`, data);
  }

  delete(id) {
    return http.delete(`/categoria/${id}`);
  }
}

export default new CategoriaService();