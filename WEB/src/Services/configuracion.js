import http from "../http-common";

class Configuracion {
  getAll() {
    return http.get("/Configuracion");
  }
  
  create(data) {
    return http.post("/Configuracion", data);
  }

  update(id, data) {
    return http.put(`/Configuracion/${id}`, data);
  }

  delete(id) {
    return http.delete(`/Configuracion/${id}`);
  }
}

export default new Configuracion();