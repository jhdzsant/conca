
class FileService {

    get(file){
        let reader = new FileReader();
        reader.readAsDataURL(file);

        return new Promise(function (resolve, reject) {
            reader.onload = () => {
                resolve({
                    content: reader.result.split(',')[1],
                    name: file.name,
                    length: file.size,
                    contentType: file.type
                })
            }
          });
    }
}